<?php
/**
 * Created by Fred Radeff
 * radeff.red
 * © 2017
 * distribution point = PDD
 */

 namespace Application\Block\Mypdd;
 use Concrete\Core\Block\BlockController;
 use Core;
 use Database;
 use UserInfo;


defined('C5_EXECUTE') or die("Access Denied.");

class Controller extends BlockController {

    protected $btInterfaceWidth = "1200";
    protected $btInterfaceHeight = "800";

    public function getBlockTypeName()
    {
        return t('Mon point de distribution');
    }

    public function getBlockTypeDescription()
    {
        return t('Infos sur mon point de distribution');
    }

    public function br2nl($str)
    {
        $str = str_replace("\r\n", "\n", $str);
        $str = str_replace("<br />\n", "\n", $str);

        return $str;
    }



    public function view()
    {

    }


}
