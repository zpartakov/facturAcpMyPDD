<?php
/*
* @package     c58ACP
* @subpackage  mypdd
* @author      Fred Radeff (fradeff@akademia.ch)
* @copyright   (C) 2017  radeff.red. All rights reserved.
* @license     https://www.gnu.org/licenses/gpl.html
* @link        https://radeff.red/free
* A block to display information for logged user of an ACP (contract farming) about their distribution point, made for CMS Concrete5 (v 8.1.2), linked with dolibarr (v. 6.0)
*/
?>
<style>
.pdd tr:nth-child(even) {
  background-color: lightyellow;
}

.pdd td {
  border: thin solid;
  padding: 10px;
}
  </style>
  <?php
//echo "hello, world!";
require_once(DIR_APPLICATION. '/' . DIRNAME_TOOLS . '/config.inc.php');
$db=mysql_connect($zedbHostname,$login,$pass) or  die("Unable  to  select  database");

$db = Loader::db();
//https://documentation.concrete5.org/developers/users-groups/reading-data-existing-users
use Concrete\Core\User\User;

$u = new User();
if ($u->isRegistered()) {
    //print 'User is logged in.';
}

$administrators = \Concrete\Core\User\Group\Group::getByID(3);

//Is the current user the root user/super user or member of admins?
$admins=0;
if ($u->isSuperUser()) {
    //print 'Yes, the user is a su!'; exit;
    $admins=1;
}
if ($u->inGroup($administrators)) {
  $admins=1;
//abandonné
  $admins=0;
}
//echo "admin? $admins"; exit;
if($admins==0) {
  //print '<p>the user is member of admin group!</p>'; exit;

/*
print $group->getGroupDisplayName(); // "Administrators"
print $group->getGroupID();
exit;

*/
/*
$groups = array();
$ids = array_keys($u->getUserGroups());
foreach($ids as $id) {
    $group = \Group::getByID($id);
    if (is_object($group)) {
        $groups[] = $group;
    }
}
print_r($groups); exit;
*/

//Retrieve the user ID of the current user
$ui=$u->getUserID();
$ui = UserInfo::getByID($ui);
if($ui>0) {
	$email= $ui->getUserEmail();
	$utilisateur= $ui->getUserName();
	//echo "<p>Bienvenue, ".$utilisateur ."; votre email est: " .$email; exit;
  //echo "<br>Vous vous êtes déjà connecté à " .$ui->getNumLogins() ." reprise";
if($ui->getNumLogins()>0){
  //echo "s";
}
//echo ", bravo!";
//echo "</p>";
//$billing_address = $ui->getAttribute('billing_address', 'display');
//$addresss = $ui->getUserContactAddress();
//$profile_private_messages_enabled = $ui->getAttribute('profile_private_messages_enabled');
//$responsable_pdd = $ui->getAttribute('responsable_pdd');
$dolibarr_uid = $ui->getAttribute('dolibarr_uid');

//echo "<br/>Adresse commandes: " .$billing_address;
//echo "<br/>profile_private_messages_enabled: " .$profile_private_messages_enabled;
//echo "<br/>Responsable Point de distribution: " .$responsable_pdd;
//echo "<br/>No identifiant dolibarr (facturation): " .$dolibarr_uid;

//extract new users from ERP dolibarr
//$dolibarr_db="ddkg_lesjardinsdecocagnech3";
$sql="
SELECT $dolibarr_db.llx_adherent.rowid, $dolibarr_db.llx_adherent.lastname, $dolibarr_db.llx_adherent.firstname,
$dolibarr_db.llx_adherent.email,
$dolibarr_db.llx_adherent.address,
$dolibarr_db.llx_adherent.zip,
$dolibarr_db.llx_adherent.town,
$dolibarr_db.llx_adherent.phone,
$dolibarr_db.llx_adherent.phone_mobile,
$dolibarr_db.llx_adherent_extrafields.ppgp,
$dolibarr_db.llx_adherent_extrafields.ptdistrib,
$dolibarr_db.llx_adherent_extrafields.resp_point,
$dolibarr_db.llx_adherent_extrafields.showlistpdd,
$dolibarr_db.llx_adherent_extrafields.dateentree,
$dolibarr_db.llx_adherent_extrafields.notes
FROM $dolibarr_db.llx_adherent_extrafields, $dolibarr_db.llx_adherent
WHERE $dolibarr_db.llx_adherent_extrafields.fk_object=$dolibarr_db.llx_adherent.rowid
AND $dolibarr_db.llx_adherent.statut=1
AND $dolibarr_db.llx_adherent.rowid=$dolibarr_uid
;";
	} else {
	echo "This block MUST be used on a page where the user has to log! User not logged! Exiting!"; exit;
}

//echo "<pre>$sql</pre>"; exit; //test the sql initial query
$blocks = $db->Execute($sql);

//loop
foreach ($blocks as $dolibarr_user) {
  $id=$dolibarr_user['rowid'];
  $nom=$dolibarr_user['lastname'];
  $prenom=$dolibarr_user['firstname'];
  $email=$dolibarr_user['email'];
  $pdd=$dolibarr_user['ptdistrib'];
  $ppgp=$dolibarr_user['ppgp'];
  $address=$dolibarr_user['address'];
  $zip=$dolibarr_user['zip'];
  $town=$dolibarr_user['town'];
  $phone=$dolibarr_user['phone'];
  $phone_mobile=$dolibarr_user['phone_mobile'];

  //echo "<h2>Vos informations comptables extraites du logiciel libre \"dolibarr\"</h2>";
  //echo "<p>#".$id . " " .$nom .", " .$prenom ." ".$email ." pdd: " .$pdd ."</p>";
}

//infos about the pdd
$sql='SELECT * FROM jos_pdds WHERE PDDINo LIKE "'.$pdd .'"';
//echo "<pre>$sql</pre>"; //test the sql initial query
$blocks = $db->Execute($sql);


foreach ($blocks as $row) {
  $PDDINo=$row['PDDINo'];
  $PDDAdr=$row['PDDAdr'];
    $PDDTexte=$row['PDDTexte'];
    $PDDNom=$row['PDDNom'];
    $PDDNoRue=$row['PDDNoRue'];
    $PDDTele=$row['PDDTele'];
    $PDDLieu=$row['PDDLieu'];
    $PDDEmail=$row['PDDEmail'];
    $PDDRem=$row['PDDRem'];
	}


echo "<div style=\"padding: 20px; float: right; background-color: lightgreen\">Tel: ".$PDDTele ."<br/>@:<a href=\"mailto:".$PDDEmail."\">".$PDDEmail."</a>
<br/>".$PDDRem."</div>";

echo "<h1>Point de distribution: " .$PDDTexte ." / " .$PDDAdr ." (# " .$PDDINo. ")</h1>";

//now, find all members of that PDD
$sql="
SELECT $dolibarr_db.llx_adherent.rowid, $dolibarr_db.llx_adherent.lastname, $dolibarr_db.llx_adherent.firstname,
$dolibarr_db.llx_adherent.email,
$dolibarr_db.llx_adherent.address,
$dolibarr_db.llx_adherent.zip,
$dolibarr_db.llx_adherent.town,
$dolibarr_db.llx_adherent.phone,
$dolibarr_db.llx_adherent.phone_perso,
$dolibarr_db.llx_adherent.phone_mobile,
$dolibarr_db.llx_adherent_extrafields.ppgp,
$dolibarr_db.llx_adherent_extrafields.ptdistrib,
$dolibarr_db.llx_adherent_extrafields.resp_point,
$dolibarr_db.llx_adherent_extrafields.showlistpdd,
$dolibarr_db.llx_adherent_extrafields.dateentree,
$dolibarr_db.llx_adherent_extrafields.notes
FROM $dolibarr_db.llx_adherent_extrafields, $dolibarr_db.llx_adherent
WHERE $dolibarr_db.llx_adherent_extrafields.fk_object=$dolibarr_db.llx_adherent.rowid
AND $dolibarr_db.llx_adherent.statut=1
AND $dolibarr_db.llx_adherent_extrafields.ptdistrib=$pdd
AND $dolibarr_db.llx_adherent_extrafields.showlistpdd=1
ORDER BY $dolibarr_db.llx_adherent_extrafields.resp_point, $dolibarr_db.llx_adherent.lastname, $dolibarr_db.llx_adherent.firstname
;";

//echo "<pre>$sql</pre>"; //test the sql initial query
$blocks = $db->Execute($sql);

echo '<div class="pdd" style="">';
echo "<h2>Les membres du point de distribution</h2>";
echo "<table style=\"width: 100%\">";
echo "<tr><th>Nom, prénom</th><th>Tel / Natel</th><th>Part</th><th>email</th></tr>";

//assign values to GP / PP
$totalPP=0; $totalGP=0;

//loop
foreach ($blocks as $dolibarr_user) {

$id=$dolibarr_user['rowid'];
$nom=$dolibarr_user['lastname'];
$prenom=$dolibarr_user['firstname'];
$email=$dolibarr_user['email'];
$pdd=$dolibarr_user['ptdistrib'];
$ppgp=$dolibarr_user['ppgp'];
//set nice labels for PPGP
if($ppgp==1) {
  $ppgplib="pp";
  $totalPP++;
}elseif($ppgp==2) {
  $ppgplib="GP";
  $totalGP++;
}else {
  $ppgplib="";
}

$address=$dolibarr_user['address'];
$zip=$dolibarr_user['zip'];
$town=$dolibarr_user['town'];
$phone=$dolibarr_user['phone'];
$phone_perso=$dolibarr_user['phone_perso'];
$phone_mobile=$dolibarr_user['phone_mobile'];
$resp_point=$dolibarr_user['resp_point'];


echo "<tr class=\"pdd\"><td class=\"pdd\">";

if($resp_point==1){
  echo "<strong>";
}

echo $nom .", " .$prenom;

if($resp_point==1){
  echo "</strong>";
}

echo "</td><td class=\"pdd\">" .$phone;
if(strlen($phone_perso)>0){
echo "&nbsp;/&nbsp;" .$phone_perso;
}
if(strlen($phone_mobile)>0){
echo "&nbsp;/&nbsp;" .$phone_mobile;
}
echo "&nbsp;</td><td class=\"pdd\">" .$ppgplib ."</td><td>";
//do not display __xx@cocagne.ch which are fake mails and display only if mail exists
if(!preg_match("/__xx@cocagne.ch/",$email) && strlen($email)>0){
  echo "<a href=\"mailto:".$email."\">@</a>";
}
echo "</td></tr>";
}

echo "<tr><td colspan=\"2\">Total GP (grandes parts)</td><td colspan=\"2\" style=\"text-align: right\">$totalGP</td></tr>";
echo "<tr><td colspan=\"2\">Total PP (petites parts)</td><td colspan=\"2\" style=\"text-align: right\">$totalPP</td></tr>";
echo "<tr><td colspan=\"2\">Total GP+PP</td><td colspan=\"2\" style=\"text-align: right\">".($totalGP+$totalPP)."</td></tr>";

echo "</table>";
echo "</div>";
}

/* else {
  echo "<p>Vous êtes membre du groupe administrateur, ce module ne s'applique pas; si vous voulez tester, <a href=\"https://cocagne.ch/c58/index.php/dashboard/users/search\">cherchez la personne</a>, cliquez sur sa ligne et choisissez ensuite \"Se connecter en tant qu'utilisateur\"; revenez ensuite sur cette page</p>";

  $ui=$u->getUserID();
  $ui = UserInfo::getByID($ui);
  	$email= $ui->getUserEmail();
  	$utilisateur= $ui->getUserName();


  echo "<p>Bienvenue, ".$utilisateur ."; votre email est: " .$email; exit;

}
*/
