# FacturAcp: block MyPDD

A block to display information for logged user of an ACP (contract farming) about their distribution point, made for CMS Concrete5 (v 8.1.2), linked with dolibarr (v. 6.0)

# this block belongs to facturAcp                                    
![facturAcp](//radeff.red/pics/acp/facturAcp.png "facturAcp")


![snapshot](//cocagne.ch/facturacp-man/lib/exe/fetch.php/monpdd.jpg)

https://gitlab.com/zpartakov/facturAcp

## Français

## English
coming

Note: the software is french-speaking orientered, welcome to any help for an english translation

## Deutsch
zu kommen

Bemerkung: der software ist nur verfügbar auf französich, willkommen fur Ubersetzung Hilfe auf deutsch

## Italiano
da venire

## Téléchargement/Download
https://gitlab.com/zpartakov/facturAcpMyPDD

## Requirements
### Minimal
The absolute minimal requirements are:
- Apache >= 2.4 http://apache.org/
- MySQL >= 5.5.3 https://www.mysql.com/downloads/
- PHP >= 5.4 http://www.php.net/
- CakePhp >= 3.x https://github.com/cakephp/cakephp/tags
- Bootstrap 3.x http://getbootstrap.com/
- jQuery 3.x http://jquery.com/
- Dolibarr >=  5.x https://github.com/Dolibarr/dolibarr
- CMS: Concrete5 >=8.1.2 http://www.concrete5.org/


### Manual
for more information please refer to our wiki

https://cocagne.ch/facturacp-man/doku.php/mypdd

### Wishlist / buglist
https://gitlab.com/zpartakov/facturAcpMyPDD/wikis/home

## Licence
facturAcp software is licenced under the GPL 3.0.
